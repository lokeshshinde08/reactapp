import React, { useState, useEffect } from "react";
import axios from "axios";
import {Link} from "react-router-dom";
const Home = () => {

    const [user, setUser] = useState([]);

    useEffect(() => {
        console.log("LOKESH");
        loadUsers();
    }, []);

    const loadUsers = async () => {
        const result = await axios.get("http://localhost:3003/users");
        setUser(result.data.reverse());
        // console.log(result);
    }
    const deleteUser = async id =>{
        await axios.delete(`http://localhost:3003/users/${id}`);
        loadUsers();
    }


    return (
        <div className="container">
            <div>
                <h1>Home page</h1>
                <table class="table border shadow">
                    <thead class="thead-dark" >
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Mobile</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">City</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        {
                            user.map((user,index) =>(
                                <tr>
                                    <th scope="row">{index+1}</th>
                                    <td>{user.fname}</td>
                                    <td>{user.lname}</td>
                                    <td>{user.zipcode}</td>
                                    {/* <td>{user.address.zipcode}</td> */}
                                    <td>{user.email}</td>
                                    <td>{user.city}</td>
                                    <td>
                                        <Link class="btn btn-primary mr-2" to={`/view/${user.id}`}>View</Link>
                                        <Link class="btn btn-outline-primary mr-2" to={`/edit/${user.id}`}>Edit</Link>
                                        <Link class="btn btn-danger"onClick={ () => deleteUser(user.id)}>Delete</Link>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>

                    
{/*                     <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Lokesh</td>
                            <td>Shinde</td>
                            <td>7354300076</td>
                            <td>lokesh@gmail.com</td>
                            <td>Pune</td>
                        </tr>
                    </tbody> */}
                </table>

            </div>

        </div>
    );
};
export default Home;