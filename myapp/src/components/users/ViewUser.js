import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";


const ViewUser = () => {

    const [user, setUser] = useState({
        fname: "",
        lname: "",
        zipcode: "",
        email: "",
        city: "",

    });

    const { id } = useParams();

    useEffect(() => {
        loadUser();
    }, []);

    const loadUser = async () => {
        const res = await axios.get(`http://localhost:3003/users/${id}`);
        setUser(res.data);
    };

    return (
        <div className="container py-4  center">

            <h1 className="display-4 head center"><strong><i><u>Contact Profile</u></i></strong></h1>
            <hr />
            <ul className="list-group w-60 center">
                <li className="list-group-item"><strong>First Name :-  </strong> {user.fname}</li>
                <li className="list-group-item"><strong>Last Name :-  </strong> {user.lname}</li>
                <li className="list-group-item"><strong>phone :-  </strong>{user.zipcode}</li>
                <li className="list-group-item"><strong>Email :-  </strong>{user.email}</li>
                <li className="list-group-item"><strong>City :-  </strong> {user.city}</li>
            </ul>
<hr/>


            <Link className="btn btn-warning center" to="/"> Home </Link>

        </div>
    );

};




export default ViewUser;