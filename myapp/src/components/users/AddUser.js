import axios from "axios";
import React, { useState } from "react";
import{useHistory} from 'react-router-dom';

const AddUser = () => {
    let history = useHistory();   
    const [user, setUser] = useState({
        fname: "",
        lname: "",
        zipcode: "",
        email: "",
        city: "",

    });
    
    const onInoutChange = e =>{
        setUser({...user,[e.target.name]:e.target.value})
    }

     const onSubmit = async e =>{
         e.preventDefault();
         await axios.post("http://localhost:3003/users",user);
         history.push("/");

     };
    return (

        <div className="container">
            <div className="w-75 mx-auto shadow-5">
                <h2 className="text-center mb-4">Register</h2>
                <form onSubmit={e => onSubmit(e)}>

                    <div className="form-group">
                        <label for="fName">First Name</label>
                        <input
                            type="text"
                            className="form-control"
                            placeholder="First Name"
                            name="fname" required
                            value={user.fname}
                            onChange={e => onInoutChange(e)}
                        />
                    </div>

                    <div className="form-group">
                        <label for="lName">Last Name</label>
                        <input type="text"
                            class="form-control"
                            id="lName"
                            placeholder="Last Name"
                            name="lname" required
                            value={user.lname}
                            onChange={e => onInoutChange(e)}
                        />
                    </div>


                    <div className="form-group">
                        <label for="mobile">Mobile Number</label>
                        <input type="tel"
                            class="form-control"
                            id="mobile"
                            placeholder="Mobile Number"
                            name="zipcode" required
                            maxlength="10"
                            value={user.zipcode}
                            onChange={e => onInoutChange(e)}
                        />
                    </div>


                    <div class="form-group">
                        <label for="Email1">Email address</label>
                        <input type="email"
                            class="form-control"
                            id="Email1"
                            placeholder="Enter email"
                            name="email"
                            value={user.email}
                            onChange={e => onInoutChange(e)}
                            

                        />

                    </div>

                    <div class="form-group">
                        <label for="City">City</label>
                        <input type="text"
                            class="form-control"
                            id="City"
                            placeholder="Enter City/Village/Town"
                            name="city"
                            value={user.city}
                            onChange={e => onInoutChange(e)}
                        />

                    </div>

                    <button type="submit" class="btn btn-primary" >Submit</button>


                </form>
            </div>



        </div>






    );
};
export default AddUser;